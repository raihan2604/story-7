# Story 7
This Gitlab repository is the result of the work from **Muhammad Raihan**

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/rabialco/story-6/badges/master/pipeline.svg)](https://gitlab.com/raihan2604/story-7/commits/master)
[![coverage report](https://gitlab.com/rabialco/story-6/badges/master/coverage.svg)](https://gitlab.com/raihan2604/story-7/commits/master)

## URL
This lab projects can be accessed from [https://mraihan26.herokuapp.com](https://mraihan26.herokuapp.com)

## Author
**Muhammad Raihan** - [raihan2604](https://gitlab.com/raihan2604)
