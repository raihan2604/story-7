$(document).ready(function(){
    $("#change-theme2").hide();
    $(".name-light").hide();
    $("#change-theme1").click(function(){
        $("body").css("background-color", "wheat");
        $("h3").css("background-color", "black");
        $("h3").css("color", "white");
        $("div").css("background-color", "black");
        $("div").css("color", "white");
        $("#change-theme2").show();
        $("#change-theme1").hide(); 
        $(".name-dark").hide();
        $(".name-light").show();       
    });
    $("#change-theme2").click(function(){
        $("body").css("background-color", "rgb(23, 21, 21)");
        $("h3").css("background-color", "white")
        $("h3").css("color", "black")
        $("div").css("background-color", "white")
        $("div").css("color", "black")
        $("#change-theme1").show();
        $("#change-theme2").hide();
        $(".name-light").hide();
        $(".name-dark").show();
    });

    $('#accordion').accordion({
        active: false,
        collapsible: true,
        animate: 200         
    });
});