from django.test import TestCase, Client, LiveServerTestCase
from django.http import HttpRequest
from django.urls import reverse, resolve
from .views import index

# Create your tests here.
class jsTets(TestCase):
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url_is_not_exist(self):
        response = Client().get('')
        self.assertFalse(response.status_code == 404)

    def test_using_landing_funct(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_using_index_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_ada_tulisan_nama(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Muhammad Raihan', html_response)

